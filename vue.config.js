module.exports = {
  devServer: {
    progress: false,
    port: 7210,
    overlay: {
      warnings: false,
      errors: true,
    },
  },
};
