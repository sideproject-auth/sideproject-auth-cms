import { checkStrategyExist } from '@/utils/usefulHelper';
import dayjs from 'dayjs';

// 轉換代號
const codeConvertHelper: any = (convertStrategyName: string, data: any) => {
  const codeStrategy: any = {
    // example
    nationality(value: any) {
      const convertStrategy: any = {
        America: '美國',
        Australian: '澳洲',
      };

      const isExist = checkStrategyExist(convertStrategy, value);
      if (!isExist) return console.log('無匹配轉換function');

      return convertStrategy[value];
    },

    belongGroup(value: any) {
      const convertStrategy: any = {
        image: '輪播/圖片',
        news: '消息',
        user: '使用者',
        authGroup: '權限群組',
        auth: '權限',
      };

      const isExist = checkStrategyExist(convertStrategy, value);
      if (!isExist) return console.log('無匹配轉換function');

      return convertStrategy[value];
    },
  };

  // 檢查是否有這個策略
  const isExist = checkStrategyExist(codeStrategy, convertStrategyName);
  if (!isExist) {
    return console.log('無此策略');
  }

  // 傳進當前值，和當前值的row資料
  // 策略模式
  return codeStrategy[convertStrategyName](data);
};

// 轉換意思
function formatValueHelper(convertStrategyName: string, value: any, column: any = {}) {
  const formatStrategy: any = {
    weekDayCode(value: string) {
      return dayjs(value).day();
    },
    weekDay(value: string, column?: any) {
      const weekDayCode: number = dayjs(value).day();

      switch (weekDayCode) {
        case 0:
          return '日';
        case 1:
          return '一';
        case 2:
          return '二';
        case 3:
          return '三';
        case 4:
          return '四';
        case 5:
          return '五';
        case 6:
          return '六';
        default:
          return '';
      }
    },

    // 00:00:00 --> -
    timeZero(value: string, column?: any) {
      if (value !== '00:00:00') {
        return value;
      } else {
        return '-';
      }
    },

    timeYMDHms(value: string, column?: any) {
      if (value === null) {
        return '';
      }

      return dayjs(value).format('YYYY-MM-DD HH:mm:ss');
    },

    timeYMD(value: string, column?: any) {
      if (value === null) {
        return '';
      }

      return dayjs(value).format('YYYY-MM-DD');
    },

    yesNoCh(value: any): string {
      switch (value) {
        case true:
        case 1:
        case 'yes':
          return '是';
        case false:
        case 0:
        case 'no':
          return '否';
        default:
          return '未知';
      }
    },
    yesNoEn(value: any): string {
      switch (value) {
        case true:
        case 1:
        case '是':
          return 'yes';
        case false:
        case 0:
        case '否':
          return 'no';
        default:
          return '未知';
      }
    },
    trueFalse(value: number | string): boolean | string {
      switch (value) {
        case 1:
        case '是':
        case 'yes':
          return true;
        case 0:
        case '否':
        case 'no':
          return false;
        default:
          return '未知';
      }
    },
  };

  return formatStrategy[convertStrategyName](value, column);
}

export { codeConvertHelper, formatValueHelper };
