import { Loading } from 'element-ui';
import Vue from 'vue';

const loadingInstance = {
  loadingInstance: null,
  startLoading() {
    this.loadingInstance = Loading.service({
      lock: true,
      text: 'Loading ...',
    });
  },
  endLoading() {
    Vue.prototype.$nextTick(() => {
      this.loadingInstance.close();
    });
  },
};

export default loadingInstance;
