const baseApiUrl = process.env.VUE_APP_BASE_API;
const testApiUrl = process.env.TEST_API_URL;

const networkConfig: any = {
  defaultConfig: {
    rootUrl: baseApiUrl,
    headers: {
      'Content-Type': 'application/json;charset=utf-8',
      Authorization: 'Bearer ',
    },
  },

  // to AWS S3
  // AWS 上傳有自己的驗證方式，寫入Authorization會報錯
  uploadImage: {
    rootUrl: '',
    headers: {
      'Content-Type': 'image/png',
    },
  },
  // custom
  placeholderConfig: {
    rootUrl: testApiUrl,
    headers: {
      'Content-Type': 'text/json;charset=utf-8',
    },
  },
};

export default networkConfig;
