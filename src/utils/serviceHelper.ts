import axios from 'axios';
import networkConfig from '@/utils/networkConfig';
import { Iresponse } from '@/interface/globalTyping';
import { badResHelper, customMsgHelper } from '@/utils/msgHelper';
// import { startLoading, endLoading } from '@/utils/loading';
import loadingInstance from '@/utils/loading';

function checkMethodValue(method: string) {
  const positionMethods: string[] = ['get', 'post', 'put', 'delete', 'patch'];
  const isValidate: boolean = positionMethods.includes(method);

  if (!isValidate) {
    return false;
  }

  return true;
}

const beforeRequest = (config: string, apiUrl: string, payload: any) => {
  const headerConfig: any = {
    headers: networkConfig[config].headers,
  };

  const token: string = localStorage.getItem('token') || '';

  // 若有 token 且 設定檔中有 Authorization 這個 key
  if (token && networkConfig[config].headers.Authorization) {
    headerConfig.headers.Authorization = 'Bearer ' + token;
  }

  // 專門寫給AWS S3的 ...
  if (config === 'uploadImage') {
    headerConfig.headers['Content-Type'] = payload.type;
  }

  let fullApiUrl: string = '';
  if (typeof config === 'string') {
    fullApiUrl = networkConfig[config].rootUrl + apiUrl;
  }

  return { headerConfig, fullApiUrl };
};

// 請求攔截器
axios.interceptors.request.use(
  function(config) {
    loadingInstance.startLoading();
    return config;
  },
  function(error) {
    return Promise.reject(error);
  },
);

const ajaxRequest = async (method: string, apiUrl: string, payload: object, config: any) => {
  const requstMethods: any = {
    async get() {
      return await axios.get(apiUrl, config);
    },
    async post() {
      return await axios.post(apiUrl, payload, config);
    },
    async patch() {
      return await axios.patch(apiUrl, payload, config);
    },
    async put() {
      return await axios.put(apiUrl, payload, config);
    },
    // axios的delete不支援帶資料 ...
    async delete() {
      return await axios.request({
        method: 'delete',
        url: apiUrl,
        data: payload,
        headers: config.headers,
      });
    },
  };

  const res: any = await requstMethods[method]();
  return res;
};

// 回應攔截器
axios.interceptors.response.use(
  function(configParams) {
    loadingInstance.endLoading();
    return configParams;
  },
  function(error: any) {
    loadingInstance.endLoading();
    badResHelper(error);
    return Promise.reject(error);
  },
);

const afterRequest = (res: any): Iresponse | null => {
  if (!res) {
    return null;
  }

  // 如果有警告訊息顯示出來
  if (res.data.warning) {
    customMsgHelper('warning', res.data.warning);
  }

  // 過濾出需要的內容
  return {
    status: res.request.status,
    header: res.headers,
    code: res.data.code,
    data: res.data.data,
  };
};

/**
 *
 * @param method ajax方法
 * @param apiUrl api路徑(去除根url)
 * @param payload 參數
 * @param header 是否使用特別設定的頭部，預設看config檔案
 */
export const serviceRequest = async (method: any, apiUrl: any, payload: any = {}, header: string = 'defaultConfig') => {
  // 檢查HTTP Verbs是否正確
  const isValidate = checkMethodValue(method);
  if (!isValidate) return;

  // 設定header和api url
  const { headerConfig, fullApiUrl } = beforeRequest(header, apiUrl, payload);

  // 送出request
  const response = await ajaxRequest(method, fullApiUrl, payload, headerConfig);

  // 回傳data
  return afterRequest(response);
};
