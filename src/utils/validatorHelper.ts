// 驗證表單

// interface
import { IvalidateParams, IvalidateResult } from '@/interface/validateTyping';

class Validator {
  public ruleList: any[] = [];

  // 封裝若干策略
  public formValidateStrategies: { [key: string]: Function } = {
    legalYMD(ruleName: string, value: any, inputName: string) {
      // 日期格式必須是 'YYYY-MM-DD'
      const YMDReg: RegExp = /\d{4}-\d{2}-\d{2}/;

      if (!YMDReg.test(value)) {
        return this.errMsg(ruleName, inputName);
      }
    },

    notSelect(ruleName: string, value: any, inputName: string) {
      if (value === null || value === -1 || value === '') {
        return this.errMsg(ruleName, inputName);
      }
    },
    isEmpty(ruleName: string, value: string, inputName: string) {
      if (value.trim() === '') {
        return this.errMsg(ruleName, inputName);
      }
    },
    lessStringLenth(ruleName: string, length: number = 6, value: string, inputName: string) {
      if (value.trim().length > length) {
        return this.errMsg(ruleName, inputName, length);
      }
    },
    stringLength(ruleName: string, length: number = 6, value: string, inputName: string) {
      if (value.trim().length < length) {
        return this.errMsg(ruleName, inputName, length);
      }
    },
    phoneFormat(ruleName: string, value: string, inputName: string) {
      const regExp = /^09\d{8}$/;
      if (!regExp.test(value)) {
        return this.errMsg(ruleName, inputName);
      }
    },
    isMamdarin(ruleName: string, value: string, inputName: string) {
      const regExp = /[^\u4E00-\u9FA5]/g;
      if (regExp.test(value)) {
        return this.errMsg(ruleName, inputName);
      }
    },
    isEnglish(ruleName: string, value: string, inputName: string) {
      const regExp = /[a-zA-Z]/g;
      if (!regExp.test(value)) {
        return this.errMsg(ruleName, inputName);
      }
    },
    emailFormat(ruleName: string, value: string, inputName: string) {
      const regExp = /^\w+((-\w+)|(\.\w+))*@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]+$/;
      if (!regExp.test(value)) {
        return this.errMsg(ruleName, inputName);
      }
    },
    isOverSomeYear(ruleName: string, yearLength: number = 6, value: string, inputName: string) {
      yearLength = Number(yearLength);

      const compareDayArr = value.split('-');
      const compareDayYMD = {
        y: Number(compareDayArr[0]),
        m: Number(compareDayArr[1]),
        d: Number(compareDayArr[2]),
      };

      const today = new Date();
      const todayYMD = {
        y: today.getFullYear(),
        m: today.getMonth() + 1,
        d: today.getDate(),
      };

      if (todayYMD.y - compareDayYMD.y < yearLength) {
        return this.errMsg(ruleName, inputName, yearLength);
      }

      if (todayYMD.y - compareDayYMD.y === yearLength) {
        if (todayYMD.m - compareDayYMD.m < 0 || todayYMD.d - compareDayYMD.d < 0) {
          return this.errMsg(ruleName, inputName, yearLength);
        }
      }
    },
  };

  errMsg(ruleName: string, inputName: string, otherInfo: any = '') {
    // 預設錯誤訊息
    const errMsg: any = {
      legalYMD() {
        return `請選擇${inputName}`;
      },
      notSelect() {
        return `請選擇${inputName}`;
      },
      isEmpty() {
        return `請輸入${inputName}`;
      },
      lessStringLenth() {
        return `${inputName}長度不可超過${otherInfo}位數`;
      },
      stringLength() {
        return `${inputName}長度需至少${otherInfo}位數`;
      },

      phoneFormat() {
        return `${inputName}格式錯誤`;
      },
      isMamdarin() {
        return `${inputName}僅可輸入中文`;
      },
      isEnglish() {
        return `${inputName}僅可輸入英文`;
      },
      emailFormat() {
        return `${inputName}僅可使用E-mail`;
      },
      isOverSomeYear() {
        return `${inputName}需滿${otherInfo}年`;
      },
    };

    return {
      ruleName,
      inputName,
      errInfo: errMsg[ruleName](),
    };
  }

  // 增加驗證規則
  add(args: any) {
    // 第三個參數有多少個規則，就跑幾次迴圈

    const { value, inputName, ruleList }: IvalidateParams = args;

    for (let i = 0; i < ruleList.length; i++) {
      let argsArr = ruleList[i].split(':'); // 如果規則帶有參數(冒號後面))

      this.ruleList.push(() => {
        const strategy: any = argsArr[0];
        argsArr.push(value);
        argsArr.push(inputName);

        return this.formValidateStrategies[strategy].apply(this, argsArr);
      });
    }
  }

  // 開始驗證
  start() {
    for (let i = 0; i < this.ruleList.length; i++) {
      const msg = this.ruleList[i]();

      if (msg) {
        return msg;
      }
    }

    return {
      ruleName: '',
      inputName: '',
      errInfo: '',
    };
  }
}

export { Validator };
