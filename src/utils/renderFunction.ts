export default {
  name: 'TableExpand',
  functional: true,
  props: {
    row: Object,
    render: Function,
    index: Number,
    config: {
      type: Object,
      default: null,
    },
  },
  render: (h: any, ctx: any) => {
    const params: any = {
      row: ctx.props.row,
      index: ctx.props.index,
    };

    if (ctx.props.config) {
      params.config = ctx.props.config;
    }

    return ctx.props.render(h, params);
  },
};
