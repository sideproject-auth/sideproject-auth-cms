import { setTime } from '@/utils/usefulHelper';

const searchConfig = [
  {
    key: 'dateRange',
    type: 'dateRangePicker',
    defaultValue: [setTime('month-subtract-1'), setTime('day-0-0')],
  },
  {
    key: 'groupName',
    type: 'input',
    placeholder: '權限群組名',
    defaultValue: '',
  },
];

const columnConfig: any[] = [
  {
    prop: '_id',
    label: 'ID',
    minWidth: 100,
    sortable: false,
    fixed: false,
    align: 'center',
    headerAlign: 'center',
    show: 1,
  },
  {
    prop: 'groupName',
    label: '權限群組名',
    minWidth: 120,

    fixed: false,
    align: 'center',
    headerAlign: 'center',
    show: 1,
  },

  {
    prop: 'creater',
    label: '創建人',
    minWidth: 100,
    sortable: false,
    fixed: false,
    align: 'center',
    headerAlign: 'center',
    show: 1,
  },
  {
    prop: 'createdAt',
    label: '創建時間',
    minWidth: 170,
    sortable: false,
    fixed: false,
    align: 'center',
    headerAlign: 'center',
    show: 1,
  },
  {
    prop: 'editedAt',
    label: '編輯時間',
    minWidth: 170,
    sortable: false,
    fixed: false,
    align: 'center',
    headerAlign: 'center',
    show: 1,
  },
];

const operationConfig: any = {
  prop: 'operation',
  label: '操作',
  minWidth: 170,
  sortable: false,
  fixed: 'right',
  align: 'center',
  headerAlign: 'center',
  show: 1,
  operationSetting: [
    { icon: 'groupUser', authCode: 'G12' },
    { icon: 'edit', authCode: 'G31' },
    { icon: 'delete', authCode: 'G41' },
  ],
};

export { columnConfig, searchConfig, operationConfig };
