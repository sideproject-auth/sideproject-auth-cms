import { setTime } from '@/utils/usefulHelper';

const searchConfig = [
  {
    key: 'belongGroup',
    type: 'selector',
    placeholder: '所屬權限',
    defaultValue: '',
    options: [
      {
        label: '輪播/圖片',
        value: 'image',
      },
      {
        label: '消息',
        value: 'news',
      },
      {
        label: '使用者',
        value: 'user',
      },
      {
        label: '權限群組',
        value: 'authGroup',
      },
      {
        label: '權限',
        value: 'auth',
      },
    ],
  },
];

const columnConfig: any[] = [
  // {
  //   prop: '_id',
  //   label: 'ID',
  //   minWidth: 100,
  //   sortable: false,
  //   fixed: false,
  //   align: 'center',
  //   headerAlign: 'center',
  //   show: 1,
  // },

  {
    prop: 'authId',
    label: '權限代號',
    minWidth: 70,
    fixed: false,
    align: 'center',
    headerAlign: 'center',
    show: 1,
  },
  {
    prop: 'belongGroupName',
    label: '所屬權限',
    minWidth: 120,
    fixed: false,
    align: 'center',
    headerAlign: 'center',
    show: 1,
  },

  {
    prop: 'desc',
    label: '權限說明',
    minWidth: 120,
    fixed: false,
    align: 'center',
    headerAlign: 'center',
    show: 1,
  },
  {
    prop: 'createdAt',
    label: '創建時間',
    minWidth: 170,
    sortable: false,
    fixed: false,
    align: 'center',
    headerAlign: 'center',
    show: 1,
  },
  {
    prop: 'editedAt',
    label: '編輯時間',
    minWidth: 170,
    sortable: false,
    fixed: false,
    align: 'center',
    headerAlign: 'center',
    show: 1,
  },
];

const operationConfig: any = {
  prop: 'operation',
  label: '操作',
  minWidth: 120,
  sortable: false,
  fixed: 'right',
  align: 'center',
  headerAlign: 'center',
  show: 1,
  operationSetting: [
    { icon: 'edit', authCode: 'A31' },
    { icon: 'delete', authCode: 'A41' },
  ],
};

export { columnConfig, searchConfig, operationConfig };
