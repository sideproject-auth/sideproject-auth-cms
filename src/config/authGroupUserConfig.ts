import { setTime } from '@/utils/usefulHelper';

const columnConfig: any[] = [
  {
    prop: '_id',
    label: 'ID',
    minWidth: 100,
    sortable: false,
    fixed: false,
    align: 'center',
    headerAlign: 'center',
    show: 1,
  },
  {
    prop: 'user',
    label: '使用者名稱',
    minWidth: 120,
    fixed: false,
    align: 'center',
    headerAlign: 'center',
    show: 1,
  },
];

const operationConfig: any = {
  prop: 'operation',
  label: '操作',
  minWidth: 100,
  sortable: false,
  fixed: 'right',
  align: 'center',
  headerAlign: 'center',
  show: 1,
  operationSetting: [{ icon: 'delete', authCode: 'U33' }],
};

export { columnConfig, operationConfig };
