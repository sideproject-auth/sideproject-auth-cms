import { setTime } from '@/utils/usefulHelper';

const searchConfig = [
  {
    key: 'dateRange',
    type: 'dateRangePicker',
    defaultValue: [setTime('month-subtract-1'), setTime('day-0-0')],
  },
  {
    key: 'title',
    type: 'input',
    placeholder: '文章標題',
    defaultValue: '',
  },
];

function columnConfig(currVueCom: any) {
  return [
    {
      prop: '_id',
      label: 'ID',
      minWidth: 100,
      sortable: false,
      fixed: false,
      align: 'center',
      headerAlign: 'center',
      show: 1,
    },
    {
      prop: 'customName',
      label: '標題',
      minWidth: 120,

      fixed: false,
      align: 'center',
      headerAlign: 'center',
      show: 1,
    },
    {
      prop: 'imageUrl',
      label: '圖片',
      minWidth: 150,
      sortable: false,
      fixed: false,
      align: 'center',
      headerAlign: 'center',
      show: 1,
      render: (h: any, params: any) => {
        return h('img', {
          class: 'img-wrapper',
          attrs: {
            src: params.row.imageUrl,
          },
          on: {
            click: () => {
              console.log('params---', params);
            },
          },
        });
      },
    },
    {
      prop: 'creater',
      label: '創建人',
      minWidth: 100,
      sortable: false,
      fixed: false,
      align: 'center',
      headerAlign: 'center',
      show: 1,
    },
    {
      prop: 'editor',
      label: '編輯人',
      minWidth: 100,
      sortable: false,
      fixed: false,
      align: 'center',
      headerAlign: 'center',
      show: 1,
    },
    {
      prop: 'createdAt',
      label: '創建時間',
      minWidth: 170,
      sortable: false,
      fixed: false,
      align: 'center',
      headerAlign: 'center',
      show: 1,
    },
    {
      prop: 'editedAt',
      label: '編輯時間',
      minWidth: 170,
      sortable: false,
      fixed: false,
      align: 'center',
      headerAlign: 'center',
      show: 1,
    },
  ];
}

const operationConfig: any = {
  prop: 'operation',
  label: '操作',
  minWidth: 120,
  sortable: false,
  fixed: 'right',
  align: 'center',
  headerAlign: 'center',
  show: 1,
  operationSetting: [
    { icon: 'edit', authCode: 'I31' },
    { icon: 'delete', authCode: 'I41' },
  ],
};

export { columnConfig, searchConfig, operationConfig };
