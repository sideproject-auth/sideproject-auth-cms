const searchConfig = [
  {
    key: 'name',
    type: 'input',
    placeholder: 'name',
    defaultValue: 'lalahaha',
  },
  {
    key: 'age',
    type: 'selector',
    placeholder: 'age',
    defaultValue: 3,
    options: [
      {
        label: '1',
        value: 1,
      },
      {
        label: '2',
        value: 2,
      },
      {
        label: '3',
        value: 3,
      },
      {
        label: '4',
        value: 4,
      },
    ],
  },
];

const columnConfig: any[] = [
  {
    prop: '123',
    label: 'haha',
    width: '80',
    minWidth: 80,
    sortable: false,
    fixed: false,
    align: 'center',
    headerAlign: 'center',
    show: 1,
  },
  {
    prop: '456',
    label: 'haha',
    width: 'auto',
    minWidth: 100,

    fixed: false,
    align: 'left',
    headerAlign: 'left',
    show: 1,
  },
  {
    prop: '789',
    label: 'haha',
    width: 'auto',
    minWidth: 100,
    sortable: false,
    fixed: false,
    align: 'left',
    headerAlign: 'left',
    show: 1,
  },
  {
    prop: '111',
    label: 'haha',
    width: 'auto',
    minWidth: 100,
    isI18n: true,

    fixed: false,
    align: 'left',
    headerAlign: 'left',
    show: 1,
  },
  {
    prop: '222',
    label: 'haha',
    width: 'auto',
    minWidth: 100,
    sortable: false,
    fixed: false,
    align: 'left',
    headerAlign: 'left',
    show: 1,
  },
];

export { columnConfig, searchConfig };
