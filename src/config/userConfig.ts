import { setTime } from '@/utils/usefulHelper';

const searchConfig = [
  {
    key: 'dateRange',
    type: 'dateRangePicker',
    defaultValue: ['', ''],
  },
  {
    key: 'user',
    type: 'input',
    placeholder: '使用者',
    defaultValue: '',
  },
];

const columnConfig: any[] = [
  {
    prop: '_id',
    label: 'ID',
    minWidth: 100,
    sortable: false,
    fixed: false,
    align: 'center',
    headerAlign: 'center',
    show: 1,
  },
  {
    prop: 'user',
    label: '使用者',
    minWidth: 120,

    fixed: false,
    align: 'center',
    headerAlign: 'center',
    show: 1,
  },

  {
    prop: 'authGroupName',
    label: '權限群組',
    minWidth: 100,
    isI18n: true,

    fixed: false,
    align: 'center',
    headerAlign: 'center',
    show: 1,
  },
  {
    prop: 'formatIsAdmin',
    label: '管理員',
    minWidth: 100,
    isI18n: true,

    fixed: false,
    align: 'center',
    headerAlign: 'center',
    cssStyle: 'statusColor',
    show: 1,
  },
  {
    prop: 'creater',
    label: '創建人',
    minWidth: 100,
    sortable: false,
    fixed: false,
    align: 'center',
    headerAlign: 'center',
    show: 1,
  },
  {
    prop: 'createdAt',
    label: '創建時間',
    minWidth: 170,
    sortable: false,
    fixed: false,
    align: 'center',
    headerAlign: 'center',
    show: 1,
  },
  {
    prop: 'editedAt',
    label: '編輯時間',
    minWidth: 170,
    sortable: false,
    fixed: false,
    align: 'center',
    headerAlign: 'center',
    show: 1,
  },
];

const operationConfig: any = {
  prop: 'operation',
  label: '操作',
  minWidth: 170,
  sortable: false,
  fixed: 'right',
  align: 'center',
  headerAlign: 'center',
  show: 1,
  operationSetting: [
    { icon: 'key', authCode: 'U31' },
    { icon: 'edit', authCode: 'U32' },
    { icon: 'delete', authCode: 'U41' },
  ],
};

export { columnConfig, searchConfig, operationConfig };
