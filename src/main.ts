// dependencies & package
import Vue from 'vue';
import App from '@/App.vue';

// router/store
import router from '@/router';
import store from '@/store';

// vue meta
import VueMeta from 'vue-meta';

// element-ui
import ElementUI from 'element-ui';
import '@/styles/preset/reset.scss';
import '@/styles/element-variables.scss';

// i18n
// import VueI18n from 'vue-i18n';
// import i18nData from '@/i18n';

Vue.config.productionTip = false; // 關掉console裡面的提示

Vue.use(ElementUI);

Vue.use(VueMeta, {
  refreshOnceOnNavigation: true,
});

Vue.mixin({
  methods: {
    validateForm(form: any) {
      const formKey: string[] = Object.keys(form);

      // 觸發事件，呼叫子組件驗證function
      // 兩個迴圈不能寫再一起是因為，要全部都驗證並提示錯誤訊息，不能只有單一個
      for (const key of formKey) {
        (this as any).$refs[key].$emit('triggerValidate');
      }

      // 檢查子組件驗證完成後的結果，有任一false，就是未通過
      for (const key of formKey) {
        const validateResult: boolean = (this as any).$refs[key].isPassValidate;

        if (!validateResult) {
          return false;
        }
      }

      return true;
    },
  },
});

new Vue({
  router,
  store,
  // i18nData
  render: h => h(App),
}).$mount('#app');
