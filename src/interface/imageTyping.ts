import { IsearchParam, Ipager } from './globalTyping';

export interface IimageModel extends Iimage, Document {}

// --- 資料庫格式 ---

export interface Iimage {
  customName: string;
  filePath: string;
  isUsed: number;
  creater: string;
  editor: string;
  createdAt: Date;
  editedAt: Date;
}

// --- API請求格式 ---

export interface IList {
  onlyLink: boolean;
}

// /carousel/getImageUrls	取得線上輪播中圖片位置
// 不需要參數

// /image/getHistoryList	取得歷史圖片

export interface IhistoryImageList {
  condition: {
    orderRule: IsearchParam;
    startDate: number;
    endDate: number;
  };
  pager: Ipager;
}

// /image/upload	新增/上傳圖片到使用中的輪播
export interface IcreateImage {
  customName: string; // 新圖名稱
  imageUrl: string;
}

// /image/edit/new	編輯圖片名字
export interface IeditImage {
  id: string;
  imageUrl: string;
  customName: string; // 圖片名稱
}

// /image/edit/new	上傳並更新使用中的圖
export interface IupdateNewImage {
  removeId: string; // 要被撤下的圖片Id
  customName: string; // 新圖名稱
}

// /image/edit/history	使用舊圖更換使用中輪播
export interface IupdateOldImage {
  removeId: string; // 要被撤下的圖片Id
  updateId: string; // 要更新上去圖片Id
}

// /image/remove	刪除使用中圖片
export interface IremoveImage {
  id: string;
}
