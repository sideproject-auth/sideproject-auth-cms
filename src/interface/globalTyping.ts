export interface Iresponse {
  status: number;
  header: any;
  code: number;
  data: any;
  warning?: string;
}

export interface IsearchParam {
  type: string;
  order: orderMethod[];
}

export interface Ipager {
  pageIndex: number;
  pageSize: number;
}

export interface Isummary {
  [key: string]: string | number;
}

export interface IpageInfo {
  pageIndex: number;
  pageSize: number;
  totalCounts: number;
}

enum orderMethod {
  ASC = 1,
  DESC = 2,
}
