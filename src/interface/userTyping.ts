import { IsearchParam, Ipager } from '@/interface/globalTyping';

// --- API請求格式 ---

// /user/login	使用者登入
export interface IuserLogin {
  user: string;
  password: string;
}

// /user/getList	取得使用者列表
export interface IuserList {
  condition: {
    beginAt: string;
    endAt: string;
    user: string;
    orderRule: IsearchParam | [];
  };
  pager: Ipager;
}

// /user/register	註冊/新增 使用者
export interface Iregister {
  user: string;
  password: string;
  authGroup: string[]; // 權限群組的id
  isAdmin: string | boolean;
}

// /user/edit	編輯使用者
export interface IeditUserPwd {
  id: string;
  password: string;
}

export interface IeditUserAuth {
  id: string;
  authGroup: string[];
  isAdmin: boolean | string;
}

// /user/remove	刪除使用者
export interface IdeleteUser {
  id: string;
}

// /user/remove/auth 刪除使用者特定權限
export interface IremoveUserAuthGroup {
  id: string;
  removeAuthGroupId: string;
}
