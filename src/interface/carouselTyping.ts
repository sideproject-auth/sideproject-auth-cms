import { IsearchParam, Ipager } from '@/interface/globalTyping';

// --- 資料庫格式 ---

export interface ICarousel {
  customName: string;
  uniqueName: string; // 收到時由node產生
  filePath: any;
  createdAt: Date;
  editedAt: Date;
  isUsed: boolean;
}

// --- API請求格式 ---

// /carousel/getImageUrls	取得線上輪播中圖片位置
// 不需要參數

// /carousel/getHistoryList	取得歷史圖片
export interface IhistoryImageList {
  condition: {
    imageName: string;
    orderRule: IsearchParam;
    startDate: number;
    endDate: number;
  };
  pager: Ipager;
}

// /carousel/upload	新增/上傳圖片到使用中的輪播
export interface IcreateImage {
  name: string; // 圖片名稱
  file: any; // 檔案
}

// /carousel/edit/new	上傳新圖並更換使用中輪播
export interface IupdateNewImage {
  order: number; // 輪播順序
  name: string; // 圖片名稱
  file: string; // 檔案
}

// /carousel/edit/history	使用舊圖更換使用中輪播
export interface IupdateOldImage {
  order: number;
  id: string;
}

// /carousel/remove	刪除使用中圖片
export interface IremoveImage {
  order: number; // 輪播順序
}
