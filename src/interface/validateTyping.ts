export interface IvalidateParams {
  value: any;
  inputName: string;
  ruleList: string[];
}

export interface IvalidateResult {
  ruleName: string;
  inputName: string;
  errInfo: string;
}
