export interface response<T> {
  code: number;
  message: string;
  data: T;
}

export interface responseErr {
  code: number;
  message: string;
}
