import { IsearchParam, Ipager } from '@/interface/globalTyping';

// --- API請求格式 ---

export interface IgetAuthList {
  condition: {
    belongGroup: string;
    orderRule: IsearchParam | [];
  };
  pager: Ipager;
}

// /auth/create	建立權限
export interface IauthCreate {
  authId: string;
  belongGroup: string;
  desc: string;
  [key: string]: string;
}

// /auth/edit	編輯權限
export interface IauthEdit {
  id: string;
  authId: string;
  belongGroup: string;
  desc: string;
}

// /auth/remove	刪除權限
export interface IauthRemove {
  id: string;
}
