// 引入type，避免function衝突
import * as type from '@/store/types';
import { formatValueHelper } from '@/utils/codeConvertHelper';

import ImageService from '@/services/imageService';
const imageService = new ImageService();

export default {
  namespaced: true,
  state: {
    imageList: {
      items: [],
      pageInfo: {
        pageIndex: 1,
        pageSize: 10,
      },
    },
  },
  getters: {
    formatImageList({ imageList }: any) {
      if (imageList.length === 0) {
        return imageList;
      }

      const cloneImageList: any = JSON.parse(JSON.stringify(imageList));

      cloneImageList.items = cloneImageList.items.map((ele: any) => {
        ele.createdAt = formatValueHelper('timeYMDHms', ele.createdAt);
        ele.editedAt = formatValueHelper('timeYMDHms', ele.editedAt);

        return ele;
      });

      return cloneImageList;
    },
  },
  mutations: {
    [type.imageList](state: any, payload: any) {
      state.imageList = payload;
    },
  },
  actions: {
    async getImageList({ commit }: any, payload: any) {
      const res: any = await imageService.getImageListAPI(payload);
      commit(type.imageList, res.data);
    },
  },
};
