// 引入type，避免function衝突
import * as type from '@/store/types';
import { formatValueHelper, codeConvertHelper } from '@/utils/codeConvertHelper';

import AuthService from '@/services/authService';
const authService = new AuthService();

export default {
  namespaced: true,
  state: {
    authList: {
      items: [],
      pageInfo: {
        pageIndex: 1,
        pageSize: 10,
      },
    },
  },
  getters: {
    formatAuthList({ authList }: any) {
      if (authList.items.length === 0) {
        return authList;
      }

      const cloneGroupList: any = JSON.parse(JSON.stringify(authList));

      cloneGroupList.items = cloneGroupList.items.map((ele: any) => {
        ele.createdAt = formatValueHelper('timeYMDHms', ele.createdAt);
        ele.editedAt = formatValueHelper('timeYMDHms', ele.editedAt);
        ele.belongGroupName = codeConvertHelper('belongGroup', ele.belongGroup);

        return ele;
      });

      return cloneGroupList;
    },
  },
  mutations: {
    [type.authList](state: any, payload: any) {
      state.authList = payload;
    },
  },
  actions: {
    async getAuthList({ commit }: any, payload: any) {
      const res: any = await authService.getAuthListAPI(payload);
      commit(type.authList, res.data);
    },
  },
};
