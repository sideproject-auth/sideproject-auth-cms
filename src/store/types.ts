export const template = 'template';

// news
export const newsList = 'newsList';
export const news = 'news';

// user
export const userList = 'userList';
export const permissionList = 'permissionList';

// group
export const groupList = 'groupList';
export const authGroupUserList = 'authGroupUserList';

// auth
export const authList = 'authList';

// image
export const imageList = 'imageList';
