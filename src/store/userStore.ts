// 引入type，避免function衝突
import * as type from '@/store/types';
import UserService from '@/services/userService';
const userService = new UserService();

import { formatValueHelper } from '@/utils/codeConvertHelper';
import dayjs from 'dayjs';

export default {
  namespaced: true,
  state: {
    userList: {
      items: [],
      pageInfo: {
        pageIndex: 1,
        pageSize: 10,
      },
    },
    permissionList: [],
  },
  getters: {
    formatUserList({ userList }: any) {
      if (userList.items.length === 0) {
        return userList;
      }

      const cloneUserList: any = JSON.parse(JSON.stringify(userList));

      cloneUserList.items = cloneUserList.items.map((ele: any) => {
        ele.authGroupName = ele.authGroupName.join();
        ele.createdAt = formatValueHelper('timeYMDHms', ele.createdAt);
        ele.editedAt = formatValueHelper('timeYMDHms', ele.editedAt);
        ele.statusColorStyle = ele.isAdmin; // 判斷css用
        ele.formatIsAdmin = formatValueHelper('yesNoCh', ele.isAdmin);

        return ele;
      });

      return cloneUserList;
    },
    hasAuth({ permissionList }: any) {
      const authObj: { [index: string]: boolean } = {};

      permissionList.forEach((ele: any) => {
        authObj[ele] = true;
      });

      return authObj;
    },
  },
  mutations: {
    [type.userList](state: any, payload: any) {
      state.userList = payload;
    },

    [type.permissionList](state: any, payload: any) {
      state.permissionList = payload;
    },
  },
  actions: {
    async getUserList({ commit }: any, payload: any) {
      const res: any = await userService.getUserListAPI(payload);
      commit(type.userList, res.data);
    },

    async getPermissionListAPI({ commit }: any, payload: any) {
      const res: any = await userService.getPermissionListAPI(payload);
      commit(type.permissionList, res.data);
    },
  },
};
