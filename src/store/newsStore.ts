// 引入type，避免function衝突
import * as type from '@/store/types';
import { formatValueHelper } from '@/utils/codeConvertHelper';

import NewsService from '@/services/newsService';
const newsService = new NewsService();

export default {
  namespaced: true,
  state: {
    newsList: {
      items: [],
      pageInfo: {
        pageIndex: 1,
        pageSize: 10,
      },
    },
    news: {},
  },
  getters: {
    formatNewsList({ newsList }: any) {
      if (newsList.length === 0) {
        return newsList;
      }

      const cloneNewsList: any = JSON.parse(JSON.stringify(newsList));

      cloneNewsList.items = cloneNewsList.items.map((ele: any) => {
        ele.createdAt = formatValueHelper('timeYMDHms', ele.createdAt);
        ele.editedAt = formatValueHelper('timeYMDHms', ele.editedAt);
        ele.statusColorStyle = ele.isShow; // 判斷css用
        ele.formatIsShow = formatValueHelper('yesNoCh', ele.isShow);

        return ele;
      });

      return cloneNewsList;
    },
  },
  mutations: {
    [type.newsList](state: any, payload: any) {
      state.newsList = payload;
    },

    [type.news](state: any, payload: any) {
      state.news = payload;
    },
  },
  actions: {
    async getNewsList({ commit }: any, payload: any) {
      const res: any = await newsService.getNewsListAPI(payload);
      commit(type.newsList, res.data);
    },

    async getNews({ commit }: any, payload: any) {
      const res: any = await newsService.getNewsAPI(payload);
      commit(type.news, res.data);
    },
  },
};
