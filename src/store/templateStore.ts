// 引入type，避免function衝突
import * as type from '@/store/types';
import { handleTestAPI } from '@/services/TestService';

export default {
  namespaced: true,
  state: {
    data: 0,
  },
  getters: {
    stateGetter(state: any) {
      return { getter: state.date };
    },
  },
  mutations: {
    [type.template](state: any, payload: any) {
      state.data = payload;
    },
  },
  actions: {
    async handleTemplate({ commit }: any) {
      const res = await handleTestAPI();
      commit(type.template, res);
    },
  },
};
