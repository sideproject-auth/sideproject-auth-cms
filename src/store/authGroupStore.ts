// 引入type，避免function衝突
import * as type from '@/store/types';
import { formatValueHelper } from '@/utils/codeConvertHelper';

import AuthGroupService from '@/services/authGroupService';
const authGroupService = new AuthGroupService();

export default {
  namespaced: true,
  state: {
    authGroupList: {
      items: [],
      pageInfo: {
        pageIndex: 1,
        pageSize: 10,
      },
    },
    authGroupUserList: {
      items: [],
      pageInfo: {
        pageIndex: 1,
        pageSize: 10,
      },
    },
  },
  getters: {
    formatAuthGroupList({ authGroupList }: any) {
      if (authGroupList.items.length === 0) {
        return authGroupList;
      }

      const cloneGroupList: any = JSON.parse(JSON.stringify(authGroupList));

      cloneGroupList.items = cloneGroupList.items.map((ele: any) => {
        ele.createdAt = formatValueHelper('timeYMDHms', ele.createdAt);
        ele.editedAt = formatValueHelper('timeYMDHms', ele.editedAt);

        return ele;
      });

      return cloneGroupList;
    },
  },
  mutations: {
    [type.groupList](state: any, payload: any) {
      state.authGroupList = payload;
    },
    [type.authGroupUserList](state: any, payload: any) {
      state.authGroupUserList = payload;
    },
  },
  actions: {
    async getAuthGroupList({ commit }: any, payload: any) {
      const res: any = await authGroupService.getAuthGroupListAPI(payload);
      commit(type.groupList, res.data);
    },

    async getAuthGroupUserList({ commit }: any, payload: any) {
      const res: any = await authGroupService.getGroupUserListAPI(payload);
      commit(type.authGroupUserList, res.data);
    },
  },
};
