export default [
  {
    path: '/images',
    name: 'images',
    iconName: 'el-icon-picture-outline',
    authCode: 'I11',
    component: () => import('@/views/Images.vue'),
  },
  {
    path: '/news',
    name: 'news',
    iconName: 'el-icon-news',
    authCode: 'N11',
    component: () => import('@/views/News.vue'),
  },
  {
    path: '/user',
    name: 'user',
    iconName: 'el-icon-user',
    authCode: 'U11',
    component: () => import('@/views/User.vue'),
  },
  {
    path: '/authGroup',
    name: 'authGroup',
    iconName: 'el-icon-set-up',
    authCode: 'G11',
    component: () => import('@/views/AuthGroup.vue'),
  },
  {
    path: '/auth',
    name: 'auth',
    iconName: 'el-icon-setting',
    authCode: 'A11',
    component: () => import('@/views/Auth.vue'),
  },
];
