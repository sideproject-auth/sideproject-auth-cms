export default [
  {
    path: '/createEditNews/:id',
    name: 'createEditNews',
    component: () => import('@/views/CreateEditNews.vue'),
  },
];
