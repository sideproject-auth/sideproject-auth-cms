import { serviceRequest } from '@/utils/serviceHelper';

class NewsService {
  public getNewsListAPI(payload = {}) {
    const apiUrl = '/news/getList';
    return serviceRequest('post', apiUrl, payload);
  }

  public getNewsAPI(payload = {}) {
    const apiUrl = '/news/getNews';
    return serviceRequest('post', apiUrl, payload);
  }

  public createNewsAPI(payload = {}) {
    const apiUrl = '/news/create';
    return serviceRequest('post', apiUrl, payload);
  }

  public editNewsAPI(payload = {}) {
    const apiUrl = '/news/edit';
    return serviceRequest('patch', apiUrl, payload);
  }

  public removeNewsAPI(payload = {}) {
    const apiUrl = '/news/remove';
    return serviceRequest('delete', apiUrl, payload);
  }
}

export default NewsService;
