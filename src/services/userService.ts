import { serviceRequest } from '@/utils/serviceHelper';

class UserService {
  loginAPI(payload = {}) {
    const apiUrl = '/user/login';
    return serviceRequest('post', apiUrl, payload);
  }

  getUserListAPI(payload = {}) {
    const apiUrl = '/user/getList';
    return serviceRequest('post', apiUrl, payload);
  }

  registerUserAPI(payload = {}) {
    const apiUrl = '/user/register';
    return serviceRequest('post', apiUrl, payload);
  }

  editUserPasswordAPI(payload = {}) {
    const apiUrl = '/user/edit/password';
    return serviceRequest('patch', apiUrl, payload);
  }

  editUserAuthAPI(payload = {}) {
    const apiUrl = '/user/edit/auth';
    return serviceRequest('patch', apiUrl, payload);
  }

  removeUserAPI(payload = {}) {
    const apiUrl = '/user/remove';
    return serviceRequest('delete', apiUrl, payload);
  }

  removeUserAuthAPI(payload = {}) {
    const apiUrl = '/user/removeAuth';
    return serviceRequest('patch', apiUrl, payload);
  }

  getPermissionListAPI(payload = {}) {
    const apiUrl = '/user/getPermissionList';
    return serviceRequest('get', apiUrl, payload);
  }
}

export default UserService;
