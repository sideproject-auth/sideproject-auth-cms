import { serviceRequest } from '@/utils/serviceHelper';

class AuthGroupService {
  public getAuthGroupListAPI(payload = {}) {
    const apiUrl = '/group/getList';
    return serviceRequest('post', apiUrl, payload);
  }

  public getGroupUserListAPI(payload = {}) {
    const apiUrl = '/group/getCurrGroupUserList';
    return serviceRequest('post', apiUrl, payload);
  }

  public createGroupAPI(payload = {}) {
    const apiUrl = '/group/create';
    return serviceRequest('post', apiUrl, payload);
  }

  public editGroupAPI(payload = {}) {
    const apiUrl = '/group/edit';
    return serviceRequest('patch', apiUrl, payload);
  }

  public removeGroupAPI(payload = {}) {
    const apiUrl = '/group/remove';
    return serviceRequest('delete', apiUrl, payload);
  }
}

export default AuthGroupService;
