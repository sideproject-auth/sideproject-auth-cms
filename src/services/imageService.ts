import { serviceRequest } from '@/utils/serviceHelper';

class ImageService {
  public getImageListAPI(payload = {}) {
    const apiUrl = '/image/getList';
    return serviceRequest('post', apiUrl, payload);
  }

  public getSignedUrl(payload = {}) {
    const apiUrl = '/image/upload';
    return serviceRequest('post', apiUrl, payload);
  }

  public uploadImageToS3(payload: any = {}) {
    const apiUrl = payload.url;
    return serviceRequest('put', apiUrl, payload.file, 'uploadImage');
  }

  public createImageAPI(payload: any = {}) {
    const apiUrl = '/image/create';
    return serviceRequest('post', apiUrl, payload);
  }

  public editImageAPI(payload: any = {}) {
    const apiUrl = '/image/edit';
    return serviceRequest('patch', apiUrl, payload);
  }

  public deleteImageAPI(payload: any = {}) {
    const apiUrl = '/image/remove';
    return serviceRequest('delete', apiUrl, payload);
  }
}

export default ImageService;
