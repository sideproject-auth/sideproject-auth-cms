import { serviceRequest } from '@/utils/serviceHelper';

class Auth {
  public getAuthListAPI(payload = {}) {
    const apiUrl = '/auth/getList';
    return serviceRequest('post', apiUrl, payload);
  }

  public createAuthAPI(payload = {}) {
    const apiUrl = '/auth/create';
    return serviceRequest('post', apiUrl, payload);
  }

  public editAuthAPI(payload = {}) {
    const apiUrl = '/auth/edit';
    return serviceRequest('patch', apiUrl, payload);
  }

  public removeAuthAPI(payload = {}) {
    const apiUrl = '/auth/remove';
    return serviceRequest('delete', apiUrl, payload);
  }
}

export default Auth;
